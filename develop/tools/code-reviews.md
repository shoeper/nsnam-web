---
title: Code reviews
layout: page
permalink: /develop/tools/code-reviews/
---
Code reviews are performed by creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) on GitLab.com.  In summary, users should create their own fork of the main code repository, push a branch containing the new feature or patch to their fork, and then use the tools on the GitLab.com web interface to push a merge request towards the main repository.

## Legacy code review tool (Rietveld)
[Rietveld](http://code.google.com/p/rietveld/) is a web-based code review tool that has been built by Google. It was originally developed as a demonstration of the Google app engine but has been widely used by many open source projects to conduct code reviews in the open.

The project is no longer using rietveld to manage code reviews, but there are some useful and interesting (unmerged) patches still present there.  We recommend anyone who would like to revive any patches and work on them further to create a new merge request and include a back link in the description to the original Rietveld review.  

