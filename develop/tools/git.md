---
title: Git
layout: page
permalink: /develop/tools/git/
---
After many years of using [Mercurial](http://mercurial.selenic.com/), ns-3 migragted in December 2018 to using [Git](https://git-scm.com).  Git is an open-source source code management tool that is available on a wide variety of platforms (windows, OSX, Linux, etc.).

ns-3 maintains Git information in its online [manual](https://www.nsnam.org/docs/manual/html/working-with-git.html).
