---
title: Regression tests
layout: page
permalink: /develop/tools/regression-tests/
---
## GitLab.com pipelines

The project uses [CI/CD pipelines](https://docs.gitlab.com/ee/ci/pipelines.html) on GitLab.com to perform a basic check of every commit.

## Jenkins

The development version of ns-3 is regularly built automatically on a variety of platforms with a number of different compilers through an instance of a [Jenkins Continuous Integration (CI)](http://jenkins-ci.org/) server.

Users who want to see the current status of these builds can browse them via [the web interface](https://ns-buildmaster.ee.washington.edu:8010/). Build errors are sent to the [ns-commits mailing list](http://mailman.isi.edu/mailman/listinfo/ns-commits). Build scripts are maintained in a [script repository](http://code.nsnam.org/jenkins).
