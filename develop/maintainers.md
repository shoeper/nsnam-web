---
title: Maintainers
layout: page
permalink: /develop/maintainers/
---
ns-3 maintainers have responsibility and authority to oversee different portions of the codebase. Maintainers work with ns-3 contributors to marshal in patches or extensions to their respective modules. They also participate in timely code reviews, bug fixing, and respond to questions on mailing lists, pertaining to their respective modules.

Maintainers coordinate with other maintainers for patches that have larger scope than their particular module. Maintainers may also, in the future, help us with platform-specific issues (e.g., ns-3 Cygwin maintainer) or packaging.

If you would like to serve as an ns-3 maintainer, please contact the [ns-developers](/developers/mailing-lists#ns-developers) list.  We have several modules
missing current/active maintainers (see below).

Our list of current and past maintainers is found [here](/about/our-team/)

The current ns-3 maintainers, by module or topic, include:

<table border="0" align="center">
  <tr>
    <th align="left">
      Module
    </th>
    
    <th align="left">
      Person(s)
    </th>
    
    <th align="left">
      Email
    </th>
  </tr>
  
  <tr>
    <td>
      antenna
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      aodv
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      applications
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      bake build system
    </td>
    
    <td>
      <a href="mailto:tomh@tomh.org">Tom Henderson</a> and <a href="mailto:adadeepak8@gmail.com">Ankit Deepak</a>
    </td>
    
    <td>
      tomh@tomh.org and adadeepak8@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      bridge
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      buildings
    </td>
    
    <td>
      <a href="mailto:bbojovic@cttc.es">Biljana Bojovic</a>
    </td>
    
    <td>
      bbojovic@cttc.es
    </td>
  </tr>
  
  <tr>
    <td>
      click
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      config-store
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      core
    </td>
    
    <td>
      <a href="mailto:barnes26@llnl.gov">Peter Barnes</a>
    </td>
    
    <td>
      barnes26@llnl.gov
    </td>
  </tr>
  
  <tr>
    <td>
      csma
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      csma-layout
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      dsdv
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      dsr
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      energy
    </td>
    
    <td>
      <a href="mailto:cristiano.tapparello@rochester.edu">Cristiano Tapparello</a>
    </td>
    
    <td>
      cristiano.tapparello@rochester.edu
    </td>
  </tr>
  
  <tr>
    <td>
      fd-net-device
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      flow-monitor
    </td>
    
    <td>
      <a href="mailto:tommaso.pecorella@unifi.it">Tommaso Pecorella</a>
    </td>
    
    <td>
      tommaso.pecorella@unifi.it
    </td>
  </tr>
  
  <tr>
    <td>
      internet (IP, ICMP, UDP)
    </td>
    
    <td>
      <a href="mailto:tpecorella@mac.com">Tommaso Pecorella</a>
    </td>
    
    <td>
      tpecorella@mac.com
    </td>
  </tr>
  
  <tr>
    <td>
      internet (routing)
    </td>
    
    <td>
      <a href="mailto:tomh@tomh.org">Tom Henderson</a>
    </td>
    
    <td>
      tomh@tomh.org
    </td>
  </tr>
  
  <tr>
    <td>
      internet (TCP)
    </td>
    
    <td>
      <a href="mailto:natale.patriciello@gmail.com">Natale Patriciello</a> and <a href="mailto:tahiliani.nitk@gmail.com">Mohit Tahiliani</a>
    </td>
    
    <td>
      natale.patriciello@gmail.com and tahiliani.nitk@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      internet-apps
    </td>
    
    <td>
      <a href="mailto:tpecorella@mac.com">Tommaso Pecorella</a>
    </td>
    
    <td>
      tpecorella@mac.com
    </td>
  </tr>
  
  <tr>
    <td>
      lr-wpan
    </td>
    
    <td>
      <a href="mailto:tpecorella@mac.com">Tommaso Pecorella</a>
    </td>
    
    <td>
      tpecorella@mac.com
    </td>
  </tr>
  
  <tr>
    <td>
      lte (PHY and MAC)
    </td>
    
    <td>
      <a href="mailto:mmiozzo@cttc.es">Marco Miozzo</a>
    </td>
    
    <td>
      mmiozzo@cttc.es
    </td>
  </tr>
  
  <tr>
    <td>
      lte (RLC and above, core, EPC)
    </td>
    
    <td>
      <a href="mailto:manuel.requena@cttc.es">Manuel Requena</a>
    </td>
    
    <td>
      manuel.requena@cttc.es
    </td>
  </tr>
  
  <tr>
    <td>
      lte (general)
    </td>
    
    <td>
      <a href="mailto:bbojovic@cttc.es">Biljana Bojovic</a> and 
      <a href="mailto:ilabdsf@gmail.com">Alexander Krotov</a> and
      <a href="mailto:zoraze.ali@cttc.es">Zoraze Ali</a> 
    </td>
    
    <td>
      bbojovic@cttc.es and ilabdsf@gmail.com and zoraze.ali@cttc.es
    </td>
  </tr>
  
  <tr>
    <td>
      mesh
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      mobility
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      mpi
    </td>
    
    <td>
      <a href="mailto:barnes26@llnl.gov">Peter Barnes</a>
    </td>
    
    <td>
      barnes26@llnl.gov
    </td>
  </tr>
  
  <tr>
    <td>
      netanim
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      network
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      nix-vector
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      olsr
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      openflow
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      point-to-point
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      point-to-point-layout
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      propagation
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      sixlowpan
    </td>
    
    <td>
      <a href="mailto:tpecorella@mac.com">Tommaso Pecorella</a>
    </td>
    
    <td>
      tpecorella@mac.com
    </td>
  </tr>
  
  <tr>
    <td>
      spectrum
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      stats
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      tap-bridge
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      test framework
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      tools
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      topology-read
    </td>
    
    <td>
      <a href="mailto:tommaso.pecorella@unifi.it">Tommaso Pecorella</a>
    </td>
    
    <td>
      tommaso.pecorella@unifi.it
    </td>
  </tr>
  
  <tr>
    <td>
      traffic-control
    </td>
    
    <td>
      <a href="mailto:stavallo@unina.it">Stefano Avallone</a> and
      <a href="mailto:tahiliani.nitk@gmail.com">Mohit Tahiliani</a>
    </td>
    
    <td>
      stavallo@unina.it and tahiliani.nitk@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      uan
    </td>
    
    <td>
      <a href="mailto:fedwar@gmail.com">Federico Guerra</a>
    </td>
    
    <td>
      fedwar@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      virtual-net-device
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      visualizer
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      wifi (lead)
    </td>
    
    <td>
      <a href="mailto:sebastien.deronne@gmail.com">Sebastien Deronne</a>
    </td>
    
    <td>
      sebastien.deronne@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      wifi (802.11ax aspects)
    </td>
    
    <td>
      <a href="mailto:sebastien.deronne@gmail.com">Sebastien Deronne</a> and <a href="mailto:stavallo@unina.it">Stefano Avallone</a> and <a href="mailto:getachew.redieteab@orange.com">Getachew Redieteab</a>
    </td>
    
    <td>
      sebastien.deronne@gmail.com and stavallo@unina.it and getachew.redieteab@orange.com
    </td>
  </tr>
  
  <tr>
    <td>
      wifi (sleep/energy aspects)
    </td>
  
    <td>
      <a href="mailto:stavallo@unina.it">Stefano Avallone</a>
    </td>
  
    <td>
      stavallo@unina.it
    </td>
  </tr> 
  
  <tr>
    <td>
      wifi (rate control aspects)
    </td>
  
    <td>
      <a href="mailto:mrichart@fing.edu.uy">Matias Richart</a>
    </td>
  
    <td>
      mrichart@fing.edu.uy
    </td>
  </tr> 
  
  <tr>
    <td>
      wimax
    </td>
    
    <td>
      None
    </td>
    
    <td>
      None
    </td>
  </tr>
  
  <tr>
    <td>
      python bindings
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a> and <a href="mailto:adadeepak8@gmail.com">Ankit Deepak</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      tutorial
    </td>
    
    <td>
      <a href="mailto:tomh@tomh.org">Tom Henderson</a>
    </td>
    
    <td>
      tomh@tomh.org
    </td>
  </tr>
  
  <tr>
    <td>
      manuals
    </td>
    
    <td>
      <a href="mailto:tomh@tomh.org">Tom Henderson</a> and <a href="mailto:barnes26@llnl.gov">Peter Barnes</a>
    </td>
    
    <td>
      tomh@tomh.org and barnes26@llnl.gov
    </td>
  </tr>
  
  <tr>
    <td>
      Doxygen
    </td>
    
    <td>
      <a href="mailto:barnes26@llnl.gov">Peter Barnes</a>
    </td>
    
    <td>
      barnes26@llnl.gov
    </td>
  </tr>
  
  <tr>
    <td>
      waf build system
    </td>
    
    <td>
      <a href="mailto:gjcarneiro@gmail.com">Gustavo Carneiro</a>
    </td>
    
    <td>
      gjcarneiro@gmail.com
    </td>
  </tr>
  
  <tr>
    <td>
      wiki
    </td>
    
    <td>
      <a href="mailto:tomh@tomh.org">Tom Henderson</a>
    </td>
    
    <td>
      tomh@tomh.org
    </td>
  </tr>

  <tr>
    <td>
      app store
    </td>
    
    <td>
      <a href="mailto:abhijithabhayam@gmail.com">Abhijith Anilkumar</a>
    </td>
    
    <td>
      abhijithabhayam@gmail.com
    </td>
  </tr>
</table>
