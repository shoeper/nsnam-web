---
layout: page
title: Documentation
permalink: /documentation/
---

This page is an entry point to the ns-3 documentation maintained by the project (other tutorials exist on external blogs and on YouTube but are not typically listed here).

Documentation is available for the [current release](/releases/latest/), [older releases](/documentation/older/), and also our [development tree](/documentation/development-tree/).

## Getting started

*   **Tutorial:** The tutorial for our latest release is available in [HTML](/docs/release/3.32/tutorial/html/index.html) and [PDF](/docs/release/3.32/tutorial/ns-3-tutorial.pdf) versions.
*   **Installation instructions:** We typically maintain this information on our [wiki](/wiki/Installation).
*   **Videos:** The ns-3 Consortium has offered training sessions at its [annual meeting](/research/wns3/)), and recorded videos from prior sessions can be found from [this page](/consortium/activities/training/).
*   **Mailing lists:** We have several [mailing lists](/support/mailing-lists/), but in particular, the [ns-3-users Google Group forum](https://groups.google.com/forum/#!forum/ns-3-users), answers many questions from people trying to get started.

## Development

Most users will need to write new simulation scripts and possibly modify or extend the ns-3 libraries to conduct their work. The three main resources for this are our reference manual, model library documentation, and our Doxygen.

*   We maintain a **reference manual** on the ns-3 core, and a separate **model library** documentation set, also in [several formats](/ns-3-32/documentation/) for our latest release.
*   All of our APIs are documented using [**Doxygen**](/docs/release/3.32/doxygen/index.html)
*   The ns-3 [coding style](/developers/contributing-code/coding-style/) documentation is maintained on this site.

## Technical reports

Some technical reports about the ns-3 models are listed below.

### Wi-Fi models

* [Technical report on ns-3 IEEE 802.11ax models](https://depts.washington.edu/funlab/wp-content/uploads/2018/11/11ax-final-report.pdf) Note: ns-3 802.11ax code discussed in this report is in preparation for future public release. 
* [Validation of Wi-Fi DCF in saturation](https://depts.washington.edu/funlab/wp-content/uploads/2015/03/ns3-TR.pdf)
* [Validation of Wi-Fi 802.11n error model](https://depts.washington.edu/funlab/wp-content/uploads/2017/05/Technical-report-on-validation-of-error-models-for-802.11n.pdf)
* [Validation of Wi-Fi OFDM error model](https://www.nsnam.org/~pei/80211ofdm.pdf)
* [Validation of Wi-Fi 802.11b error model](https://www.nsnam.org/~pei/80211b.pdf)

## Related projects

*   **Direct Code Execution**: Documentation on the ns-3 [Direct Code Execution](/overview/projects/direct-code-execution) environment is also linked from this site.
*   **[Netanim](/wiki/NetAnim)**: A network animator for ns-3.
*   **[Bake](/docs/bake/tutorial/html/)**: The package management tool for advanced ns-3 builds.
*   **[ns-2](/support/faq/ns2-ns3/)**

## Miscellaneous

Not finding what you are looking for? Have a look at the [ns-3 wiki](/wiki), where such topics as current development, release planning, summer projects, contributed code, FAQs and HOWTOs. Not everything is linked from the masthead so try entering keywords in the search box.

We have a number of [**other archived documents**](/documentation/presentations/) such as older tutorials or talks presented about ns-3.
