---
title: Download
layout: page
permalink: /releases/ns-3-25/download/
---
Please click the following link: [ns-allinone-3.25](https://www.nsnam.org/release/ns-allinone-3.25.tar.bz2).

A source code patch to update ns-3.24.1 release to ns-3.25 release is available [here](https://www.nsnam.org/release/patches/ns-3.24.1-to-ns-3.25.patch), and a patch to upgrade from ns-3.24 to the maintenance release ns-3.24.1 can be found [here](https://www.nsnam.org/release/patches/ns-3.24-to-ns-3.24.1.patch). Other patches to migrate older versions of ns-3 (back to ns-3.17) to the latest version can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.
