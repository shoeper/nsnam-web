---
title: Documentation
layout: page
permalink: /releases/ns-3-25/documentation/
---
  * **Tutorial:** an introduction into downloading, setting up, and using builtin models:
      * **English:** [pdf](/docs/release/3.25/tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.25/tutorial/singlehtml/index.html), [html (split page)](/docs/release/3.25/tutorial/html/index.html)
      * **Brazilian Portuguese:** [pdf](/docs/release/3.25/tutorial-pt-br/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.25/tutorial-pt-br/singlehtml/index.html), [html (split page)](/docs/release/3.25/tutorial-pt-br/html/index.html) (note: not significantly updated since ns-3.13)
      * **Greek:** [pdf](/docs/translations/greek-tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/translations/greek-tutorial/singlehtml/index.html), [html (split page)](/docs/translations/greek-tutorial/html/index.html) (note: not significantly updated since ns-3.22)
  * **Manual:** an in-depth coverage of the architecture and core of ns-3: [pdf](/docs/release/3.25/manual/ns-3-manual.pdf), [html (single page)](/docs/release/3.25/manual/singlehtml/index.html), [html (split page)](/docs/release/3.25/manual/html/index.html)
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: [pdf](/docs/release/3.25/models/ns-3-model-library.pdf), [html (single page)](/docs/release/3.25/models/singlehtml/index.html), [html (split page)](/docs/release/3.25/models/html/index.html)
  * [Release Errata](http://www.nsnam.org/wiki/Ns-3.25-errata)
      * [API Documentation](/docs/release/3.25/doxygen/index.html):&nbsp; Coverage of the C++ APIs using Doxygen.
      * Documentation of the [Bake](http://www.nsnam.org/docs/bake/tutorial/html/index.html) integration tool
          * Documentation of the [Direct Code Execution](http://www.nsnam.org/overview/projects/direct-code-execution/) 1.8 release
              * [API Documentation](/docs/dce/release/1.8/doxygen/index.html):&nbsp; Coverage of the APIs using Doxygen.
              * Manual: [html (single page)](/docs/dce/release/1.8/manual/singlehtml/index.html), [html (split page)](/docs/dce/release/1.8/manual/html/index.html)
