---
title: ns-3.31
layout: page
permalink: /releases/ns-3-31/
---
  <p>
    <b>ns-3.31</b> was released on June 27, 2020. ns-3.31 includes the following new features:
  </p>

  <ul>
  <li> 
    Models for pathloss, chanel condition, fast fading, and antenna arrays based on 3GPP TR 38.901 model.
  </li>
  <li> 
    Matrix-based channel model base class to support antenna arrays
  </li>
  <li> 
    Random walk mobility model that does not allow nodes to enter buildings
  </li>
  <li> 
   A datacenter TCP (DCTCP) model
  </li>
  <li> 
    An option to enable IPv4 hash-based multicast duplicate packet detection (DPD) based on RFC 6621
  </li>
  <li> 
    LTE trace sources for uplink PSD and RBs
  </li>
  <li> 
    A new interface to SQLite
  </li>
  <li> 
    Support for the Cake set-associative hash to the FqCoDel queue disc
  </li>
  <li> 
    Support for ECN marking to CoDel and FqCoDel queue discs
  </li>
  <li> 
    A Wi-Fi Bianchi (saturation) example with a comparable MATLAB model
  </li>
  <li> 
    The idealized Wi-Fi rate control includes better support for different MIMO modes and channel widths
  </li>
  <li> 
    The CommandLine facility can now add the Usage message to the Doxygen for the program
  </li>
  <li> 
    A new approach to run example programs as regression tests
  </li>

  <p>
    The release also includes numerous bug fixes and small improvements, listed in the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.31/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
  </p>
  <p>
      The ns-3.31 release <b>download</b> is available from <a href="/releases/ns-allinone-3.31.tar.bz2">this link</a>.
  </p>

  <ul>
    <li>
      A patch to upgrade from ns-3.30.1 to ns-3.31 can be found <a href="/releases/patches/ns-3.30.1-to-ns-3.31.patch">here</a>
    </li>
    <li>
      What has changed since ns-3.30? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.31/CHANGES.html"> changes</a> and <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.31/RELEASE_NOTES"> RELEASE_NOTES</a> pages for ns-3.30. </li>
  </ul>
  <p>
    The <b>documentation</b> is available in several formats from <a href="/releases/ns-3-31/documentation">this link</a>.
  </p>
  <ul>
    <li>
      Errata containing late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
    </li>
  </ul>
