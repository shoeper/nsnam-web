---
title: Download
layout: page
permalink: /releases/ns-3-30/download/
---
Please click the following link to download ns-3.30.1, released September 18, 2019.  ns-3.30.1 updates the original ns-3.30 release from August 2019: 

  * [ns-allinone-3.30.1](/releases/ns-allinone-3.30.1.tar.bz2) (compressed source code archive)

For users of ns-3.29 release, two patches are available to update code to ns-3.30.1.  A source code patch to update ns-3.29 release to ns-3.30 release is available [here](/release/patches/ns-3.29-to-ns-3.30.patch).  Following that patch, a source code patch to update ns-3.30 release to ns-3.30.1 release is available [here](/release/patches/ns-3.30-to-ns-3.30.1.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to the latest version can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-3.30.1 release file is 7a94f4d17b6d21d10d80693df59437eff918462b.
