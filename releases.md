---
layout: page
title: Releases
permalink: /releases/
---

ns-3 releases are made two or three times per year, and are primarily delivered as compressed source archives.  

* The [latest](/releases/ns-3-32/) release including source code, list of new features, and links to documentation.

* [Errata](/wiki/Errata) for releases is maintained on the project wiki.

* [Older releases](/releases/older/) are archived for those who might need them.
