---
title: Sponsors
layout: page
permalink: /about/sponsors/
---
This page recognizes those individuals and organizations who have provided services or funding to support the open source project.

Organizations who wish to support ns-3 are encouraged to join the [ns-3 Consortium](/consortium).

# Federal funding #

ns-3 received community infrastructure development awards from the U.S. 
National Science Foundation (NSF).  Several collaborative awards were granted,
but the lead awards were made to the University of Washington:
* CNS-0551686: CRI: Developing the Next Generation Open Source Network Simulator ns-3 (2006-10), and
* CNS-0958139: CI-ADDO-EN: Frameworks for ns-3 (2010-15).

In addition, the ns-3 development at Inria Sophia Antipolis was funded by
the French government.

# Programs and services #

* Git hosting is provided by the [Open Source Program at GitLab.com](https://about.gitlab.com/handbook/marketing/community-relations/opensource-program).

* Website hosting is provided by [Georgia Tech](https://www.ece.gatech.edu/) and the [University of Washington](https://www.ece.uw.edu/).

* The [Free/Open Source Project of MacStadium](https://www.macstadium.com/opensource) provides a Mac for regression testing.

* [GlobalSign](https://www.globalsign.com/en-ph/ssl/ssl-open-source/) provides the project with SSL certificates.

* [USC ISI](https://www.isi.edu/) and Google Forums provide the mailing lists.

* [Google Summer of Code](https://summerofcode.withgoogle.com) provides funding stipends to participating organizations.

# Annual meeting hosts #

Since 2013, the following organizations have hosted and supported the project's annual meeting:

* **2013:** Inria Sophia Antipolis
* **2014:** Georgia Tech
* **2015:** CTTC
* **2016:** University of Washington
* **2017:** INESC TEC
* **2018:** NITK Surathkal
* **2019:** DINFO, Firenze University

# Google Summer of Code mentors #

Many Google Summer of Code mentors have voluntarily donated their mentoring
stipends to the project.  Prior to 2014, this was done on a voluntary basis,
and since 2014, the project has requested mentors to donate their stipends.

## 2013 and earlier ##

* [CTTC](http://www.cttc.es) mentoring team (Nicola Baldo, Marco Miozzo) for [GSoC 2012](http://www.google-melange.com/gsoc/homepage/google/gsoc2012)
* [CTTC](http://www.cttc.es) mentoring team (Nicola Baldo, Marco Miozzo, Manuel Requena, and Jaime Ferragut) for [GSoC 2013](http://www.google-melange.com/gsoc/homepage/google/gsoc2013)
* Daniel Camara
* Joe Kopena
* Tommaso Pecorella
* Josh Pelkey
* Guillaume Rémy

## 2014 ##
* Nicola Baldo
* Marco Miozzo
* Tom Henderson
* Tommaso Pecorella
* Dave Taht

## 2015 ##
* Nicola Baldo
* Marco Miozzo
* Tom Henderson
* Vedran Miletic
* Tommaso Pecorolla
* Peter Barnes

## 2017 ##
* Tom Henderson
* Tommaso Pecorella
* Biljana Bojovic
* Mohit Tahiliani
* Stefano Avallone

## 2018 ##
* Tom Henderson
* Tommaso Pecorella
* Mohit Tahiliani
* Dizhi Zhou
* Matthieu Coudron

## 2019 ##
* Tom Henderson
* Tommaso Pecorella
* Mohit Tahiliani
* Abhijith Anilkumar
* Ankit Deepak
* Vivek Jain
* Natale Patriciello
