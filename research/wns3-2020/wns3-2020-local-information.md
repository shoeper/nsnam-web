---
layout: page
title: WNS3 2020 Local Information
permalink: /research/wns3/wns3-2020/local-information/
---

<div style="background-color:#cccccc; text-align:left; vertical-align: middle; padding:5px 47px;"> <b>Note:</b> This page will be updated once WNS3 is rescheduled.</div>

# General Chair

Contact Richard Rouil, NIST (richard.rouil at nist.gov) for any specific questions about local information.

# Location of the meeting

The meeting will be held at NIST in [Gaithersburg, MD](https://www.nist.gov/about-nist/visit/getting-nist-gaithersburg).

# Local hotel information

Information about local hotel room blocks will be posted at a later date.

# Visa requirements

The official web form to check if you need a visa, and outlining the procedures to obtain one if so, is found at [this link](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visitor.html).  The B-1 Business Visa type should be used.  Citizens of some countries are eligible for a [visa waiver](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visa-waiver-program.html).

Attendees who need a letter of support for a visa application should contact the general chair.

# NIST registration and security requirements

NIST is a federal facility and requires additional information from conference attendees prior to their visit. Please be sure to submit your information on the NIST Access Site (link to be posted at a later date). Unfortunately, any attendee that does not submit their info on the NIST site will be denied access at the NIST Gate. NIST Registration closes on June 8.

Please also note that NIST security is unable to clear attendees who are citizens on the U.S. Department of State's [list of Countries of State Sponsors of Terrorism](https://www.state.gov/state-sponsors-of-terrorism/).  Those countries are currently: Democratic People’s Republic of Korea (North Korea), Iran, Sudan and Syria. 

Photo identification must be presented at the NIST gate to be admitted to the event. International attendees are required to present a passport. Attendees must wear their conference badge at all times while on the campus. There is no on-site registration for events held at NIST.

* For Non-US Citizens:  Please present a valid passport for photo identification.

* For US Permanent Residents: Please present green card for photo identification.

* For US Citizens: Please present state-issued driver's license. Regarding Real-ID requirements, all states are in compliance or have an extension through October 2020. For a list of alternative forms of ID, please visit [NIST campus access and security](https://www.nist.gov/about-nist/visit/campus-access-and-security).

A link to the NIST Access Site will be provided as part of the registration process.

# Visitor information links

* Please review the [NIST Visitor page](https://www.nist.gov/about-nist/visit).

* Another NIST conference page has some [useful links](https://www-nlpir.nist.gov/projects/tv2019/attending.html).
