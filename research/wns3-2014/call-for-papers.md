---
layout: page
title: Call for Papers
permalink: /research/wns3/wns3-2014/call-for-papers/
---
The Workshop on ns-3 (WNS3) is a one day workshop to be held on May 7, 2014, on the campus of the Georgia Institute of Technology in Atlanta GA. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities. The workshop is held in technical cooperation with the [European Alliance of Innovation (EAI)](http://eai.eu/) and the [Institute for Computer Sciences, Social Informatics and Telecommunications Engineering (ICST)](http://icst.org/).

- <a href="http://eai.eu" target="_blank">EAI</a>
- <a href="http://isct.org/" target="_blank">ICST</a> 

WNS3 invites authors to submit original high quality papers presenting different aspects of developing and using ns-3. The Workshop is making arrangements with the ACM Digital Library to have papers published in the Digital Library.

Topics of interest include, but are not limited to, the following:

  * new models, devices, protocols and applications for ns-3
  * using ns-3 in modern networking research
  * comparison with other network simulators and emulators
  * speed and scalability issues for ns-3
  * multiprocessor and distributed simulation with ns-3, including the use of GPUs
  * validation of ns-3 models
  * credibility and reproducibility issues for ns-3 simulations
  * user experience issues of ns-3
  * frameworks for the definition and automation of ns-3 simulations
  * post-processing, visualisation and statistical analysis tools for ns-3
  * models ported from other simulators to ns-3 and models ported from ns-3 to other simulation environments
  * using real code for simulation with ns-3 and using ns-3 code in network applications
  * integration of ns-3 with testbeds, emulators, and other simulators or tools
  * using ns-3 API from programming languages other than C++ or Python
  * porting ns-3 to unsupported platforms
  * network emulation with ns-3
  * ns-3 documentation issues
  * ns-3 community issues
  * using ns-3 in education and teaching to use ns-3

Papers must be written in English and must not exceed 8 pages. Every paper will be peer-reviewed. At least one author of each accepted paper must register and present the work at the conference.

### Submission instructions

Authors should submit papers through [EasyChair](https://www.easychair.org/conferences/?conf=wns30) in PDF format, complying with [ACM Conference Proceedings format, Option 2 (tighter ‘Alternate’ style)](http://www.acm.org/sigs/publications/proceedings-templates). Submitted papers must not have been submitted for review or published (partially or completely) elsewhere.

### Acceptance Criteria

Papers will be accepted based on the relevance, novelty, and impact of the contribution, as well as the quality of writing and presentation.

Authors presenting new ns-3 models, frameworks, integration setups, etc. are encouraged to include all traditional parts of a scientific paper: introduction, motivation, related work, assumptions, verification and validation, conclusions and references. As a general rule, papers that only document source code will be rejected.

Authors presenting networking research using ns-3 are encouraged to follow best simulation practices and focus particularly on the credibility and reproducibility of simulation results. As a general rule, papers that only present simulation results obtained without any significant modification to the ns-3 simulator, and without a focus on simulation methodology, will be considered out of scope and rejected.

We strongly encourage authors of all papers, demonstrations, and posters to include links to relevant source code and instructions on how to use it. This will make contributions more useful for the ns-3 community. For papers presenting new ns-3 models, a link to the respective code review issue will be a plus.

Please do not hesitate to contact the workshop chairs if you are uncertain whether your submission falls within the scope of the workshop.

### Copyright Policy

Authors will have their paper published in the ACM Digital Library, which will require copyright transfer to ACM under their [normal terms and conditions](http://www.acm.org/publications/policies/copyright_policy/). In the spirit of open source, we encourage authors of published papers to exercise their right to publish author-prepared versions on their respective home page, or on a publicly accessible server of their employer. We will provide links from the WNS3 program on the ns-3 web site to the author-prepared version.

### Demonstrations and Posters

In addition to the regular paper track, we are organising an exhibition-style demonstration and poster session, not to be published on the conference proceedings. The aim is to foster interactive discussions on work-in-progress, new problem statements, ideas for future development related to ns-3, and display innovative prototypes.

To propose a demonstration or poster, please submit a one or two page long extended abstract in PDF format to the TPC chairs. For demonstrations, the abstract should include the basic idea, the scope, and significance of the same. Additionally, please provide information about the equipment to be used for the demonstration and whether any special arrangements will be needed. Be as specific as possible in describing what you will demonstrate. Please include an estimate of the space, and setup time needed for your demonstration.

Accepted poster and demo abstracts will be published on the ns-3 web site. At least one author of each accepted demo/poster must register and present the work at the conference.

### Awards

One Best Paper and one Best Demo or Poster will be selected through peer reviews and will be announced at the workshop. Depending on the number of submissions and accepted papers, a Best Student Paper Award may be awarded (to be determined). To be eligible for the Best Student Paper Award, the lead author and presenter must be a student.

### Registration

Registration fees have not been confirmed but will be posted at a later date, and are expected to cover the cost of conducting the event at Georgia Tech.

### Other Events

We are planning additional events during this week:

  * Training on ns-3 will be offered on Monday and Tuesday, May 5-6.
  * Developer meetings and coding sprints will be held on Thursday and Friday, May 8-9

More information on these will be announced at a future date.

### Important Dates

Papers submission deadline : Sunday, February 9, 2014, 11:59pm EST (now closed)

Notification of acceptance : February 28, 2014

Camera-ready deadline : April 4, 2014

Demos and posters proposal deadline : March 14, 2014

Workshop in Atlanta : May 7, 2014
