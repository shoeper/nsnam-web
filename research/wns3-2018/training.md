---
layout: page
title: WNS3 2018 Training
permalink: /research/wns3/wns3-2018/training/
---

ns-3 training sessions are fee-based events intended to both educate new users and raise some funding for the open source project. Training will be held on Monday June 11 and Tuesday June 12, prior to the Workshop on ns-3. Three or four ns-3 maintainers will conduct the training.

Monday June 11 will be an introduction to ns-3, for users just starting with ns-3.  We will provide a comprehensive overview of the most commonly used parts of ns-3.

Tuesday June 12 will contain four more advanced sessions, including sessions on Wi-Fi, LTE, traffic control, and advanced modes of ns-3 (DCE, MPI, emulation).

Please view the [training flyer](https://www.nsnam.org/docs/consortium/training/ns-3-training-2018.pdf) for more information, as well as the [wiki page](https://www.nsnam.org/wiki/AnnualTraining2018) for the latest updates.
