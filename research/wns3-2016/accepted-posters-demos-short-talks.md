---
layout: page
title: Accepted Posters, Demos, and Short Talks
permalink: /research/wns3/wns3-2016/accepted-posters-demos-short-talks/
---
Thirteen posters and demos were presented (no short talks). Some authors have made their posters or demo abstracts available (hyperlinked below).

  * Ankit Deepak, Shravya K.S., Radhesh Anand M.C., and Mohit P. Tahiliani. &nbsp;[IPv6 Support for OLSR in ns-3](http://www.nsnam.org/workshops/wns3-2016/posters/IPv6-support-for-OLSR.pdf).
  * N&eacute;stor J. Hern&aacute;ndez M., Jeppe Pihl, Janus Heide, Jeppe Krigslund, Morten V. Pedersen, Péter Vingelmann, Daniel E. Lucani and Frank H.P. Fitzek. &nbsp;[Wurf.it: &nbsp;A Network Coding Reliable Multicast Content Streaming Solution &#8211; NS-3 Simulations and Implementation](http://www.nsnam.org/workshops/wns3-2016/posters/hernandez-demo-paper.pdf).
  * Sangeeth K. and&nbsp;Maneesha Vindodini Ramesh. &nbsp;Intergrating NS3 to Remote Triggered WSN Testbed.
  * Tanguy Ropitault. &nbsp;Real-time Visualization in ns-3 using a Web Browser.
  * Amir Modarresi, Siddharth Gangadhar, Truc Anh N. Nguyen and James P. G. Sterbenz. &nbsp;H-TCP Implementation in ns-3.
  * Matthieu Coudron and Stefano Secci. &nbsp;[Per Node Clocks to Simulate Clock Desynchronization in ns-3](http://www.nsnam.org/workshops/wns3-2016/posters/per-node-clock-abstract.pdf)
  * Chuanji Zhang, Jared Ivey and George Riley. &nbsp;[Link Colonization in SDN Simulation Animation for ns-3 using NetAnim.](http://www.nsnam.org/workshops/wns3-2016/posters/link-colorization-sdn.pdf)
  * Richard Rouil, Fernando J. Cintr&oacute;n, Aziza Ben Mosbah and Samantha Gamboa Quintiliani. &nbsp;[An LTE Device-to-Device module for ns-3.](http://www.nsnam.org/workshops/wns3-2016/posters/wns3_2016_LTE_D2D_NIST.pdf)
  * Jeffrey M. Young. &nbsp;[Power Grid Communication Planning and Modeling Tool Code Generator for ns-3.](http://www.nsnam.org/workshops/wns3-2016/posters/WNS3-PGCPMT-poster.pdf)
  * Bernhard G&auml;de, Johannes B. Huber, Andreas M. Lehmann and J&ouml;rg Deutschmann. &nbsp;A Power Line Communication Topology Module for ns-3 and DCE.
  * Alberto Gallegos, Taku Noguchi, Tomoko Izumi and Yoshio Nakatani. &nbsp;[Maximum Amount Shortest Path (MASP) Routing Protocol Implementation in ns-3.](http://www.nsnam.org/workshops/wns3-2016/posters/nsnamPosterAlbertoGallegos.pdf)
  * Adrian E. Conway. &nbsp;[Nephel3: A Cloud-Based ns-3 Code Generation Service.](http://www.nsnam.org/workshops/wns3-2016/posters/Conway-poster.pdf)
  * Keerthi Ganta, Isha Khadka, Md Moshfequr Rahman, Truc Anh N Nguyen and James P.G. Sterbenz. &nbsp;An Implementation of TCP-Illinois in ns-3.
