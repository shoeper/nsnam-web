---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2016/program-committee/
---
### **General Chair**

  * Tom Henderson, University of Washington, <tomhend@u.washington.edu>

### **Technical Program Committee Co-Chairs**

  * Brian Swenson, Georgia Tech Research Institute, <bpswenson@gmail.com>
  * Hajime Tazaki, IIJ Innovation Institute, <tazaki@wide.ad.jp>

### **Proceedings Chair**

  * Eric Gamess, Universidad Central de Venezuela <egamess@gmail.com>

### **Technical Program Committee Members**

  * Alex Afanasyev, UCLA
  * Ram&oacute;n Ag&uuml;ero, University of Cantabria, Santander
  * Peter D. Barnes, Lawrence Livermore National Laboratory
  * Christopher Carothers, Rensselaer Polytechnic Institute
  * Bow-Nan Cheng, MIT Lincoln Laboratory
  * Sebastien Deronne, Alcatel-Lucent Bell
  * David Ediger, GTRI
  * Eric Gamess, Universidad Central de Venezuela, Caracas, Venezuela
  * Lorenza Giupponi, CTTC
  * <span style="line-height: 1.5;">Sam Jansen, StarLeaf</span>
  * Kevin Jin, Illinois Institute of Tech
  * Mathieu Lacage, Alcmeon
  * Jason Liu, Florida International University
  * Margaret Loper, GTRI
  * Marco Miozzo, CTTC
  * Carlos Moreno, Universidad Central de Venezuela, Caracas, Venezuela
  * David Nicol, University of Illinois
  * Luiz Felipe Perrone, Bucknell
  * Ken Renard, Army Research Lab
  * Manuel Ricardo, INESC Porto
  * George F Riley, Georgia Tech
  * Damien Saucez, INRIA
  * Brian P Swenson, Georgia Tech Research Institute
  * Mohit P. Tahiliani, National Institute of Technology Karnataka India
  * Cristiano Tapparello, University of Rochester
  * Hajime Tazaki, IIJ Innovation Institute
  * Thierry Turletti, INRIA
