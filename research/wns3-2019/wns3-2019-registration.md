---
layout: page
title: Registration 
permalink: /research/wns3/wns3-2019/registration/
---

Registration is being handled by the University of Washington Conference Services, on behalf of the [ns-3 Consortium](/consortium/) (the sponsor of the annual meeting). Registration fees cover the cost of holding the meeting, and any extra funding is provided to the the ns-3 Consortium for future use by the open source project. 

Registration is required for attending ns-3 training (Monday and Tuesday), the Workshop on ns-3 (Wednesday), and the Workshop on Next-Generation Wireless (Friday). No registration is required to attend the activities (ns-3 Consortium Annual Meeting, other software development meetings) on Thursday.

Discount rates are available to attendees from a governmental facility (e.g. a national lab), academic institution, or other non-profit organization. Student rates are available to students meeting [the IEEE criteria for student membership](https://www.ieee.org/membership/join/index.html#students).

To register, please visit **[the UW registration site](https://washington.irisregistration.com/Form/NS-3)** and select the appropriate registration category and the events of interest. Registration is open through Friday June 14.
