---
id: 2663
title: SCTP support added to ns-3 DCE
date: 2014-01-15T14:48:41+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2663
permalink: /news/sctp-support-added-to-ns-3-dce/
categories:
  - News
---
Support for the [Stream Control Transmission Protocol (SCTP)](http://www.ietf.org/rfc/rfc2960.txt) has been added to the ns-3 [Direct Code Execution (DCE)](http://www.nsnam.org/overview/projects/direct-code-execution/) framework. SCTP is an alternative transport protocol to TCP, heavily studied in the research community and supported by many vendors. With this upgrade (announced [here](http://mailman.isi.edu/pipermail/ns-developers/2014-January/011675.html)) Linux SCTP kernel implementation is now enabled (made available at the DCE API) for ns-3 DCE users.