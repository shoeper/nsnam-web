---
id: 3931
title: George Riley, In Memoriam
date: 2018-06-22T00:33:53+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3931
permalink: /news/george-riley-in-memoriam/
categories:
  - News
---
On June 20, 2018, ns-3 project co-founder George Riley passed away from cancer. George was intimately involved in ns-3&#8217;s early development, both directly and through his students. 

As a professor at Georgia Tech, George&#8217;s research focused on efficient and scalable simulations, and he and his PADS research group became involved with ns-2, developing a patch for ns-2 called PDNS (parallel/distributed ns). He also developed the Georgia Tech Network Simulator (GTNetS) around the same time; GTNetS was a packet-level simulator written in C++ with an emphasis on scalable, parallel simulations. The ns-3 simulator design drew from both of these predecessor tools, and several ns-3 features (the internet module architecture, nix-vector routing, the mpi module, BRITE routing support, the OnOffApplication and BulkSendApplication, the TCP RTT estimator) trace back to these original efforts. One can observe from George&#8217;s technical contributions [*] that simulation scalability and efficiency were common themes for his work.

George was also a strong advocate for education and ease-of-use with ns-3. He often reminded us to &#8216;keep it simple&#8217;, probably drawing on his experience in using GTNetS and ns-3 for many years while teaching at Georgia Tech. He regularly offered a graduate course in which students learned the fundamentals of discrete-event network simulation through the use of GTNetS and ns-3. George and his student John Abraham also created the Netanim network animator for visualizing ns-3 simulation output.

We will miss him, and we offer condolences to his friends and family.

[*] https://www.researchgate.net/scientific-contributions/12150170\_George\_F_Riley