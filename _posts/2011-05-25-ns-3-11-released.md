---
id: 1318
title: ns-3.11 released
date: 2011-05-25T04:35:04+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1318
permalink: /news/ns-3-11-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.11, featuring the addition of support for both the [Click Modular Router](http://read.cs.ucla.edu/click/click) and [OpenFlow Software Implementation Distribution (OFSID)](http://www.openflow.org/wk/index.php/OpenFlowMPLS), is the latest release of ns-3 and is immediately available for [download](http://www.nsnam.org/releases/ns-allinone-3.11.tar.bz2).  With over 35 bug fixes, ns-3.11 also introduces several new user-visible features:

  * The build system has been modularized, and the source code reorganized, to allow for modular libraries instead of a single monolithic ns-3 library. User programs now link a number of smaller, per-module libraries depending on the dependencies expressed to the build system. Source code is now being maintained in individual modules with consistent directory structures. Python bindings have also been modularized, and the bindings are now generated into a &#8216;ns&#8217; namespace instead of &#8216;ns3&#8217; for the old (monolithic) bindings.

  * int64x64_t is a new type which allows portable and easy to write arithmetic calculations that require a high degree of fractional precision.

  * An interface to the Click Modular Router and an Ipv4ClickRouting class has been added, to allow a node to use Click for external routing.

  * An interface to an OpenFlow software implementation distribution has been added to allow the simulation of OpenFlow switches in ns-3.

Full details are available in the [RELEASE_NOTES](http://code.nsnam.org/ns-3.11/raw-file/051a6016785d/RELEASE_NOTES).