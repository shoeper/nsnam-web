---
id: 3665
title: Google Summer of Code 2017
date: 2017-02-27T17:59:14+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3665
permalink: /news/google-summer-of-code-2017/
categories:
  - Events
  - News
---
ns-3 is participating in GSoC 2017! We were happy to learn today that we will be participating in the 2017 [Google Summer of Code](https://developers.google.com/open-source/gsoc/). This program is a great opportunity for students to learn a bit about software engineering and open source projects, and for our project community to grow. Interested students are encouraged to interact with the project through the main project mailing list, [ns-developers@isi.edu](http://www.nsnam.org/developers/tools/mailing-lists/), and to review our [wiki](https://www.nsnam.org/wiki/GSOC2017Projects). Students will have until April 3 to develop a project proposal and submit it to Google.