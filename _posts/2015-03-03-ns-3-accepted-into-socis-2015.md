---
id: 3119
title: ns-3 accepted into SOCIS 2015
date: 2015-03-03T00:09:47+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3119
permalink: /news/ns-3-accepted-into-socis-2015/
categories:
  - Events
  - News
---
ns-3 has been selected to participate in the 2015 [European Space Agency Summer of Code](http://sophia.estec.esa.int/socis2015/) program. This program is another opportunity for students to participate in open source for the summer, and with this program, all projects should have a connection to space application. The application deadline has not yet been announced. Please note that participation is restricted to students enrolled at universites in selected countries; please see [the eligibility criteria](http://sophia.estec.esa.int/socis2015/faq#socis_elig_student_who). Please subscribe to the ns-developers mailing list and monitor  [this wiki page](http://www.nsnam.org/wiki/SOCIS2015Projects) for more information.