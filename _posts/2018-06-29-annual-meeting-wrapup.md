---
id: 3935
title: Annual meeting wrapup
date: 2018-06-29T14:46:39+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3935
permalink: /news/annual-meeting-wrapup/
categories:
  - Events
  - News
---
Over 60 authors, students, and ns-3 maintainers met at NITK Surathkal from June 11-15 for the <a href="https://www.nsnam.org/overview/wns3/wns3-2018/" target="_blank">10th annual Workshop on ns-3</a>, training sessions, and a brief educational workshop hosted by the workshop general chair, Mohit Tahiliani. The meeting is organized by the ns-3 Consortium, which held its annual plenary meeting on June 14. The Workshop on ns-3 featured eleven paper presentations on Wednesday, from which Amina Sljivo accepted the award for best paper regarding the extension of the ns-3 802.11ah module, and nine poster and demo presentations the following morning. Prof. Bhaskaran Raman, IIT Bombay, opened the workshop with a keynote on &#8220;Smart Classrooms: Technology-Aids for Effective Teaching and Learning.&#8221; The meeting later toured the Center for System Design (CSD) at NITK Surathkal courtesy of Prof. K. V. Gangadharan, and representatives from Arista Networks and Criterion Networks presented talks. Next year&#8217;s annual meeting is planned for June 2019 in Florence, Italy.