---
layout: post
title:  "Registration for ns-3 annual meeting now open"
date:   2019-04-03 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
The project will be holding its annual meeting during the week of 17 June in Florence, Italy.  [Registration](/research/wns3/wns3-2019/registration/) is now open for ns-3 training, the Workshop on ns-3, and the Workshop on Next-Generation Wireless in ns-3. 
