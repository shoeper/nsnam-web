---
id: 2989
title: 2015 Workshop on ns-3 announced
date: 2014-11-04T02:04:35+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2989
permalink: /news/2015-workshop-on-ns-3-announced/
categories:
  - Events
  - News
---
The NS-3 Consortium is organizing the 2015 edition of the [Workshop on ns-3 (WNS3)](http://www.nsnam.org/overview/wns3/wns3-2015/call-for-papers/), a one day workshop to be held on May 13, 2015, at [CTTC](http://www.cttc.es) in Castelldefels (Barcelona), Spain. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 invites authors to submit original high quality papers presenting different aspects of developing and using ns-3. The Workshop is making arrangements with the ACM Digital Library and possibly other digital libraries to publish the final papers.

WNS3 will be part of a week-long series of events in Barcelona including ns-3 training, the consortium annual meeting, and developer meetings.