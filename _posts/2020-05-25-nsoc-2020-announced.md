---
title: ns-3 NSoC students announced
date: 2020-05-25T18:00:00+00:00
author: tomh
layout: post
permalink: /news/nsoc-students-announced-2020/
categories:
  - News
---
Two additional summer projects have been organized as part of the [ns-3 Summer of Code](https://www.nsnam.org/wiki/Summer_Projects#ns-3_Summer_of_Code_2020)-- an unpaid internship that we will operate similarly to Google Summer of Code.

Rahul Bothra recently graduated from BITS Pilani, is working professionally, and has open source and GSoC experience with Sugar Labs.  Rahul will be working on a project to develop models related to community wireless networking.  

Harsha Sharma is a final year undergraduate student at IIT Roorkee, and will be working on a project to develop software and best practices to manage ns-3 experiments related to the IETF L4S experiment.  We plan to use Davide Magrin's SEM software from GSoC 2018. 
