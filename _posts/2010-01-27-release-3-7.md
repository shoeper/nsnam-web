---
id: 479
title: Release 3.7
date: 2010-01-27T08:07:34+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=479
permalink: /news/release-3-7/
categories:
  - News
  - ns-3 Releases
---
## Availability

This release is immediately available from [here](http://www.nsnam.org/release/ns-allinone-3.7.tar.bz2).

## Supported platforms

ns-3.7 has been tested on the following platforms:

  * linux x86 gcc 4.4.0, 4.3.2, 4.2, 4.1.1, 4.1 and 3.4.6 (debug and optimized)
  * linux x86_64 gcc 4.4.0, 4.3.2, 4.2.4, 4.2.3, 4.2.1, 4.1.3, 3.4.6 (debug and optimized)
  * MacOS X ppc gcc 4.0.x and 4.2.x (debug and optimized)
  * cygwin gcc 3.4.4 (debug only), gcc 4.3.2 (debug and optimized)

## Unofficially supported platform

mingw gcc 3.4.5 (debug only)

Not all ns-3 options are available on all platforms; consult the [Installation](/wiki/index.php/Installation) page for more information.

## New user-visible features

  * Ad hoc On-Demand Distance Vector (AODV) routing model (RFC 3561)
  * IPv6 extensions support to add IPv6 extensions and options. Two examples (fragmentation and loose routing) are available.
  * NetAnim interface: Provides an interface to the Qt-based NetAnim animator, which supports static, point-to-point topology-based packet animations.
  * New topology helpers have been introduced &#8211; PointToPointDumbbellHelper &#8211; PointToPointGridHelper &#8211; PointToPointStarHelper &#8211; CsmaStarHelper
  * Equal-cost multipath for global routing: Enables quagga's equal cost multipath for Ipv4GlobalRouting, and adds an attribute that can enable it with random packet distribution policy across equal cost routes.
  * Binding sockets to devices: A method analogous to a SO_BINDTODEVICE socket option has been introduced to class Socket
  * Object::DoStart: Users who need to complete their object setup at the start of a simulation can override this virtual method, perform their adhoc setup, and then, must chain up to their parent.
  * Ipv4::IsDestinationAddress method added to support checks of whether a destination address should be accepted as one of the host's own addresses.
  * UniformDiscPositionAllocator added; distributes uniformly the nodes within a disc of given radius.
  * ChannelNumber attribute added to YansWifiPhy. Now it is possible to setup wifi channel using WifiPhyHelper::Set() method.
  * WaypointMobilityModel provides a method to add mobility as a set of (time, position) pairs
  * 802.11p WiFi standards
  * UDP Client/Server application
  * Support transactions in the SQLite output interface, making it usable for larger amounts of data

## Bugs fixed

The following lists many of the bugs that were fixed since ns-3.6, in many cases referencing the Bugzilla bug number:

  * bug [752](/bugzilla/show_bug.cgi?id=752): Object::DoStart is not executed for objects created at t > 0
  * bug [767](/bugzilla/show_bug.cgi?id=767): Incorrect modulation for 802.11a modes
  * bug [725](/bugzilla/show_bug.cgi?id=725): wifi fragmentation and RTS cannot be used at the same time
  * bug [782](/bugzilla/show_bug.cgi?id=782): CreateTap () requires IP address in modes other than CONFIGURE_LOCAL.
  * bug [769](/bugzilla/show_bug.cgi?id=769): Queue::GetTotalReceived{Bytes,Packets}() broken
  * bug [738](/bugzilla/show_bug.cgi?id=738) ReceiveErrorModel called too late
  * Fix NSC improper response to FIN
  * Fixed bug in serialization of PbbAddressBlock.
  * Fix bug [780](/bugzilla/show_bug.cgi?id=780) (problem in RoutingTableComputation with asymetric links), while adding debugging methods to OLSR.
  * bug [759](/bugzilla/show_bug.cgi?id=759): Ipv6 uses wrong outgoing interface.
  * bug [770](/bugzilla/show_bug.cgi?id=770): IPv6 size calculation for unknown options is wrong.
  * bug [771](/bugzilla/show_bug.cgi?id=771): Radvd does not set ttl value.
  * Fix bug [606](/bugzilla/show_bug.cgi?id=606): Arp depends on IP routing system
  * pad out CSMA payloads to 46 bytes if needed
  * Drop CSMA packets with CRC errors, rescan, dox tweaks
  * Add FCS capability to CSMA
  * Mesh:Dot11s: fixed airtime metric
  * Get emu working again: Add Dix/Llc option, add and use contextual realtime schedule ops, don't refcount realtime simulator impl
  * bug [695](/bugzilla/show_bug.cgi?id=695) &#8211; DcfManager::UpdateBackoff () uses slow HighPrecision::Div()
  * bug [674](/bugzilla/show_bug.cgi?id=674) &#8211; EIFS is not handled correctly in DcfManager::GetAccessGrantStart
  * bug [739](/bugzilla/show_bug.cgi?id=739) &#8211; OLSR: Strange HTime value in HELLO messages
  * bug [746](/bugzilla/show_bug.cgi?id=746) &#8211; UDP source address is not set to bound address
  * bug [735](/bugzilla/show_bug.cgi?id=735) Update Olsr for local delivery
  * bug [740](/bugzilla/show_bug.cgi?id=740) OLSR MprCompute () works wrong: fixed
  * bug [729](/bugzilla/show_bug.cgi?id=729) Enable IPv6 over PPP.
  * bug [645](/bugzilla/show_bug.cgi?id=645): fixes for opening stats file with OMNeT++
  * bug [689](/bugzilla/show_bug.cgi?id=689): default energy detection and CCA thresholds are changed to be more realistic.
  * bug [733](/bugzilla/show_bug.cgi?id=733): OLSR MPR Computation give incorrect result
  * Mesh: HWMP: fixed proactive routes
  * Mesh: fixed FLAME PATH_UPDATE procedure, fixed mesh.cc

## Known issues

ns-3 builds have been known to fail on the following platforms:

  * gcc 3.3 and earlier
  * optimized builds on gcc 3.4.4 and 3.4.5
  * optimized builds on linux x86 gcc 4.0.x