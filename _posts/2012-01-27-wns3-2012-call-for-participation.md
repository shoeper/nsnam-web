---
id: 1562
title: WNS3 2012 Call for Participation
date: 2012-01-27T23:06:18+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1562
permalink: /events/wns3-2012-call-for-participation/
categories:
  - Events
---
This is a reminder that the [Workshop on ns-3](http://www.nsnam.org/wns3/wns3-2012/) will be held on March 23, 2012 in Desenzano del Garda, Italy. A single day  [developers&#8217; meeting](https://www.nsnam.org/wiki/index.php/DevelMeetingMarch2012) follows the workshop.

The list of papers accepted for the presentation at the workshop can be
  
found [here](http://www.nsnam.org/wns3/wns3-2012/accepted-papers/). Registration is open [here](http://www.simutools.org/2012/General/Registration). Registration as a member is based on ACM or IEEE membership.

Notice that in addition to the regular paper track, we are organizing an
  
exhibition-style demonstration and poster session, not to be published on
  
the conference proceedings. The aim is to foster interactive discussions
  
on work-in-progress, new problem statements, ideas for future development
  
related to ns-3, and display innovative prototypes. To propose a
  
demonstration or poster, please submit a one or two page long extended
  
abstract in PDF format to the TPC chairs. For demonstrations, the abstract
  
should include the basic idea, the scope, and significance of the same.
  
Additionally, please provide information about the equipment to be used
  
for the demonstration and whether any special arrangements will be needed.
  
Be as specific as possible in describing what you will demonstrate. Please
  
include an estimate of the space, and setup time needed for your
  
demonstration.

Demonstration and poster proposal deadline is February 24, 2012.

Best regards,
  
Pavel Boyko and Lalith Suresh, WNS3 PC chairs