---
id: 3498
title: ns-3 accepted to ESA Summer of Code in Space (SOCIS) program
date: 2016-04-22T17:00:01+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3498
permalink: /news/ns-3-accepted-to-esa-summer-of-code-in-space-socis-program/
categories:
  - Events
  - News
---
The ns-3 project has been selected to participate in the 2016 edition of the European Space Agency (ESA) Summer of Code in Space (SOCIS) program. This marks the fourth consecutive year that ns-3 has been selected. SOCIS aims at offering student developers stipends to write code for various space-related open source software projects. Through SOCIS, accepted student applicants are paired with a mentor or mentors from the participating projects, thus gaining exposure to real-world software development scenarios. In turn, the participating projects are able to bring in new developers. The students eligibility is explained in the program website. However, the most important part is that they must be studying in an institution in one of the <a href="http://www.esa.int/About_Us/Welcome_to_ESA/New_Member_States" target="_blank">ESA Member States</a>. Note that the eligibility requirement is about the institution, not about the student nationality.

Our ideas webpage is at <a href="https://www.nsnam.org/wiki/SOCIS2016Projects" target="_blank">https://www.nsnam.org/wiki/SOCIS2016Projects</a>. The application deadline is May 15. More informations about SOCIS can be found on the <a href="http://sophia.estec.esa.int/socis/" target="_blank">SOCIS website</a>.