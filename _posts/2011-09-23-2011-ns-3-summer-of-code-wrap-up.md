---
id: 1425
title: 2011 ns-3 Summer of Code wrap up
date: 2011-09-23T09:42:54+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1425
permalink: /news/2011-ns-3-summer-of-code-wrap-up/
categories:
  - News
---
The 2011 [ns-3 Summer of Code (NSoC)](http://www.nsnam.org/wiki/index.php/NSOC2011AcceptedProjects) has come to an end, with all three of our students having produced code that are being reviewed for merge. We would like to thank everyone from the ns-3 team who contributed project ideas, helped us organise the programme, and support the students with feedback.

We asked mentors to provide a short summary of the students&#8217; projects, which is provided below.

Atishay Jain, mentored by Tom Henderson and Mitch Watrous, worked on providing IPv6 Global Routing support for ns-3. The project aimed at extending IPv4 global routing to IPv6, but early on it was recognized that a limitation of using/testing the code on larger topologies was going to be the lack of an automated IPv6 address generator. Atishay brought this part of the project to a successful review with a pending merge (http://codereview.appspot.com/4812043/). The remainder of his code at
  
http://code.nsnam.org/atishay/ns-3-ipv6-global-routing/ will continue to be worked on after NSoC.

Pankaj Gupta, mentored by Giuseppe Piro and Francesco Capozzi, developed the Radio Resource Control (RRC) entity for the 3GPP Long Term Evolution (LTE) model. LTE represents, nowadays, an important emerging 4G mobile broadband technology thus making tools that allow to simulate LTE networks a necessary resource for both researches and industries. Within the LTE technology, the RRC entity holds a necessary role. As part of his NSoC project, Pankaj has developed some RRC control messages that can be exchanged between eNB and UE (and vice versa) and RRC procedures able to create/send/receive/process these messages. In particular, Pankaj has developed (i) system configuration messages, including MIB, SIB1, and SIB2 parameters, (ii) paging messages, and (iii) connection control messages. All of these messages have been obtained by extending the IdealControlMessage class already available into the LTE module. Finally, Pankaj started to write procedure for creating, sending/receiving and processing these messages. To this aim, he has modified a lot the RRC entity by creating two different instances, one for the UE and one for the eNB. Moreover, a dedicated structure for storing RRC parameters configurable by RRC control messages) has been created for both UE and eNB. At this moment, Pankaj is working for finishing the implementation of RRC procedures in order to obtain a final mergable version of his
  
work.

Ashwin Narayan, mentored by Ruben Merz and Lalith Suresh, worked on implementing Click-MAC extensions to ns-3, which would allow ns-3-click users to make use of Click&#8217;s Wifi elements in their simulations. Ashwin achieved this by implementing support for Madwifi like monitor mode for ns-3&#8217;s Wifi model, and added support for per-packet transmission settings based on Radiotap. This means that Click can now control the tx-settings of a WifiNetDevice on a per packet basis. Ashwin&#8217;s work is complete and is now pending review at http://codereview.appspot.com/4890044/. We hope he will continue his involvement with the ns-3 community.

Thanks to the students for participating in this programme. We hope to get these projects merged in time for ns-3.13.