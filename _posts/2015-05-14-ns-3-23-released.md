---
id: 3219
title: ns-3.23 released
date: 2015-05-14T23:19:15+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3219
permalink: /news/ns-3-23-released/
categories:
  - News
  - ns-3 Releases
---
<a href=http://www.nsnam.org/ns-3.23>ns-3.23</a> has been released in coordination with [DCE version 1.6](https://www.nsnam.org/overview/projects/direct-code-execution/dce-1-6/) and netanim-3.106. The release includes a television transmitter (interference) model, support for geographic to cartesian coordinate conversion, updates to the mesh module to make it more standards-conformant, and several Wi-Fi enhancements mainly related to 802.11n models. The DCE 1.6 version includes a bug fix (Bug 2015) and an internal API update for the Linux kernel (i.e., libsim-linux.so). netanim-3.106 adds support for Qt 5 with gcc (tested up to Qt 5.4). This netanim release also makes an enhancement to the detection of invalid trace files, and for nodes that are rendered with images, the image is now centered on the node&#8217;s underlying coordinate.