---
title: Previous members
layout: page
permalink: /consortium/members/previous-members/
---
<p>A different ns-3 Consortium agreement was in effect from 2012-2018.  The
following additional organizations belonged to that consortium.</p>

<a href="http://www.bucknell.edu/Engineering.xml" target="_blank"><img src="/wp-content/uploads/2013/09/BUWordmark.png" width="150" alt="Bucknell University" />

<a href="http://www.llnl.gov/" target="_blank"><img src="/wp-content/uploads/2013/10/llnl-logo.gif" width="250" alt="Lawrence Livermore National Laboratory"></a> 

[**<font size="3">CMMB Vision</font>**](http://www.cmmbvision.com/) (member through 2020)
