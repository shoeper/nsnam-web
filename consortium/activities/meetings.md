---
title: Meetings
layout: page
permalink: /consortium/activities/meetings/
---
This page provides meeting materials for the current ns-3 Consortium, starting
in June 2019.

 * Sept 21, 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/minutes-092120.pdf" target="_blank">advisory board meeting minutes</a>
 * Sept 3, 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/minutes-090320.pdf" target="_blank">advisory board meeting minutes</a>
 * June 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/ns-3-consortium-talk-2020-final.pdf" target="_blank">annual meeting slides</a>
 * June 2019:  <a href="https://www.nsnam.org/wp-content/uploads/2019/ns-3-annual-meeting-slides.pdf" target="_blank">annual meeting slides</a>
